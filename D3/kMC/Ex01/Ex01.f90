MODULE variables
  USE iso_c_binding
  IMPLICIT NONE
  !
  INTEGER( c_int ), DIMENSION(:), POINTER :: site, nneig, neig, nevt, spec, event_site
  !
  REAL( c_double ), DIMENSION(:), POINTER :: rate, prop, event_rate, pressure, masse
  !
  ! ::: EVENT
  INTEGER( c_int ), DIMENSION(:), POINTER :: init_state, final_state
  REAL( c_double ), DIMENSION(:), POINTER :: ebarrier, de, f0
  REAL( c_double ), DIMENSION(:,:), POINTER :: ebond
  !
  INTEGER( c_int ) :: save_site
  !
END MODULE variables

! .....	.........................................................................................

    SUBROUTINE read_event( obj ) BIND( C )
      USE iso_c_binding
      USE derived_types
      USE sub_new_types
      USE errors
      USE variables
      IMPLICIT NONE

      TYPE( KMC_type ), INTENT( inout ) :: obj
      CHARACTER (len=500,kind=c_char)   :: string
      INTEGER( c_int )                  :: u0, i, id, ios, nevent, npress
      LOGICAL                           :: EOF
      !
      CHARACTER (len=1, kind=c_char)  :: delims
      CHARACTER (len=100, kind=c_char), DIMENSION (50) :: args
      INTEGER( c_int ) :: nargs
      !
      !  ::: Init save_site
      !
      save_site = 0
      !
      !  ::: Lecture of state and rate of each node
      !
      OPEN( newunit=u0, file=TRIM(obj% input_event), iostat=ios )
      IF ( ios /= 0 ) CALL error( "input_event doesn't open!!" )
      !
      EOF = .FALSE.
      DO WHILE ( .NOT.EOF )
         !
         CALL read_line( u0, string, EOF )
         CALL parse( TRIM(string), delims, args, nargs )
         !write (*,*) "Lu : ", nargs, (i,trim(args(i)), i=1,nargs)
         !
         ! ::: Lecture Of Event
         !
         IF ( args(1) == "Number_of_event" ) THEN
            READ( args(2), '(i5)' ) nevent
            CALL builder_event_type( obj% event, nevent )
            !exit
            !
            CALL link_int1_ptr( obj% event% ptr_i_state,   init_state,  obj% event% nevent)
            CALL link_int1_ptr( obj% event% ptr_f_state,   final_state, obj% event% nevent)
            CALL link_real1_ptr( obj% event% ptr_f0,       f0,          obj% event% nevent )
            CALL link_real1_ptr( obj% event% ptr_ebarrier, ebarrier,    obj% event% nevent)
            CALL link_real1_ptr( obj% event% ptr_de,       de,          obj% event% nevent)
            !
            DO i = 1,obj% event% nevent
               !
               READ (u0,*) id, init_state(id), final_state(id), f0(id), ebarrier(id), de(id)
               WRITE (*,*) id, init_state(id), final_state(id), f0(id), ebarrier(id), de(id)
               !
            ENDDO
            !
         ENDIF
         !
         ! ::: Lecture Of Partial Pressure Parameters
         !
         IF ( args(1) == "partial_pressure" ) THEN
            !
            READ( args(2), '(i5)' ) npress
            IF ( npress /= obj% npressure ) CALL warning( "WARNING npressure /= partial_pressure" )
            !
            CALL link_real1_ptr( obj% ptr_pressure, pressure, obj% npressure)
            CALL link_real1_ptr( obj% ptr_masse, masse, obj% npressure)
            !
            DO i = 1,npress
               READ( u0, * ) id, pressure( id ), masse( id )
               PRINT*, "Pressure", id, pressure( id ), masse( id )
            ENDDO
            !
         ENDIF
         !
      ENDDO
      !
      !
      CLOSE( u0 )
      !stop " read_event..."
      !
    END SUBROUTINE read_event
! .................................................................................................

    SUBROUTINE event_rate_calc( obj ) BIND( C )
      USE iso_c_binding
      USE derived_types
      USE sub_new_types
      USE errors
      USE variables
      IMPLICIT NONE

      TYPE( KMC_type ), INTENT( inout ) :: obj
      INTEGER                           :: i, jn, j0, istate, ievt, j, is
      REAL(KIND(1.d0))                  :: sum
      !
      CALL link_int1_ptr( obj% ptr_site,             site,        obj% tot_sites )
      CALL link_int1_ptr( obj% ptr_nneig,            nneig,       obj% tot_sites )
      CALL link_int1_ptr( obj% ptr_nevt,             nevt,        obj% tot_sites )
      CALL link_int1_ptr( obj% event% ptr_i_state,   init_state,  obj% event% nevent )
      CALL link_int1_ptr( obj% event% ptr_f_state,   final_state, obj% event% nevent )
      CALL link_real1_ptr( obj% event% ptr_f0,       f0,          obj% event% nevent )
      CALL link_real1_ptr( obj% event% ptr_ebarrier, ebarrier,    obj% event% nevent )
      CALL link_real1_ptr( obj% event% ptr_de,       de,          obj% event% nevent )
      CALL link_real1_ptr( obj% ptr_rate,            rate,        obj% tot_sites )
      !
      CALL link_int1_ptr( obj% ptr_neig,           neig,        nvois*obj% tot_sites )
      CALL link_int1_ptr( obj% ptr_event_site,     event_site,  nvois*obj% tot_sites )
      CALL link_real1_ptr( obj% ptr_event_rate,    event_rate,  nvois*obj% tot_sites )

      CALL link_real1_ptr( obj% ptr_masse, masse, obj%npressure )

      !
      ! ::: We have 1 event by site :
      !    Adsorption or Desorption
      !

      ! Initialize some vector
      nevt(:) = 1         ! It's the same as the "special" elements of "event_rate" and "event_site"

      DO is = 1,obj% tot_sites
      !
         i = is ! This variable makes us able to use this function at initialization time and for every state
         IF (save_site/=0) i = save_site ! We are going to do it only for the site we just changed
         j0 = nneig(i) ! Starting point of the info of each site in vectors event_rate and event_site
         istate = site(i)
         !
         ! Initialize some vectors
         event_rate(j0) = 1.d0
         event_site(j0) = 1
         !
         !
         sum = 0.d0
         DO jn = 1, nevt(i)
            DO j =1, obj%event%nevent
               IF (istate == init_state(j) ) ievt = j
            ENDDO
            event_site(j0 + jn) = ievt
            event_rate(j0 + jn) = ebarrier(ievt)
            sum = sum + event_rate(j0+jn) !real(ievt, kind(1.d0))
         END DO
         rate(i) = sum
         IF (save_site/=0) EXIT
         !
      ENDDO
      !
      !
    END SUBROUTINE event_rate_calc
! .................................................................................................

    SUBROUTINE choose_event( struc, isite, ievent ) BIND( C )
      USE iso_c_binding
      USE derived_types
      USE sub_new_types
      USE random
      USE variables
      IMPLICIT NONE
      TYPE( KMC_type ), INTENT( inout ) :: struc
      INTEGER( c_int ), INTENT( inout ) :: isite, ievent
      !
      INTEGER( c_int ) :: i, jn, j0
      REAL( c_double ) :: rsum, rdn, rrdn
      !
      CALL link_int1_ptr( struc% ptr_nevt,       nevt,       struc% tot_sites )
      CALL link_int1_ptr( struc% ptr_nneig,     nneig,       struc% tot_sites )
      CALL link_real1_ptr( struc% ptr_event_rate, event_rate, nvois*struc% tot_sites )
      CALL link_int1_ptr( struc% ptr_event_site,   event_site, nvois*struc% tot_sites )
      !
      isite = 0 ; ievent = 0
      CALL random_NUMBER( rdn )
      rrdn = rdn*struc% sum_rate ! Multiply the random number with the total sum of the rates of all possible jumps (6 in this case)
      struc% rand_rate = rrdn
      !
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Suppose we have 3 sites and 3 possible states and 6 different rates:
      ! 0->1: r01
      ! 0->2: r02
      ! 1->0: r10
      ! 1->2: r12
      ! 2->0: r20
      ! 2->1: r21
      ! Once one site is in one state it can go to two different states, so for each site
      ! there're two rates associated. This way the array named "event_rate" consists in three groups of
      ! three elements each. The first element of each group is the number of possible jumps/changes
      ! (which is 2 in this case) and then it follows the rates of the allowed jumps/changes.
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !
      rsum = 0.0 ! CAREFUL: You initialize "rsum" only once in the whole loop
      DO i = 1,struc% tot_sites ! Go over all the sites
         !
         j0 = nneig( i )
         DO jn = 1,nevt(i) ! Go over all the rates of the possible jumps/changes
         !  !
            ievent = jn
            rsum = rsum + event_rate( j0 + jn ) ! Sum in "rsum" the rates of the possible jumps of each site
            IF ( rsum > rrdn ) EXIT ! Check that the sum exceeds or not the random (normalized) number
            ! If it exceeds "rrdn" we choose that jump as the one we have to perform
         !  !
         ENDDO
         !
         isite = i
         IF ( rsum > rrdn ) EXIT
      ENDDO
      IF ( rsum <= rrdn ) WRITE (*,*) " PB choose event...", rsum,struc% sum_rate
      !
      save_site = isite
      !
    END SUBROUTINE choose_event
! .................................................................................................

    SUBROUTINE event_applied( struc, is, jn ) BIND( C )
      USE iso_c_binding
      USE derived_types
      USE sub_new_types
      USE errors
      USE variables
      IMPLICIT NONE
      TYPE( KMC_type )               :: struc
      INTEGER( c_int ), INTENT( in ) :: is,   &  ! Site selected
                                        jn       ! "ievent" of site "is" selected
      INTEGER( c_int )               :: jevt, j0

      CALL link_int1_ptr( struc% ptr_site,             site,             struc% tot_sites )
      CALL link_int1_ptr( struc% event% ptr_i_state,   init_state,       struc% tot_sites )
      CALL link_int1_ptr( struc% event% ptr_f_state,   final_state,      struc% tot_sites )
      CALL link_int1_ptr( struc% ptr_event_site,       event_site,       nvois*struc% tot_sites )
      CALL link_int1_ptr( struc% ptr_nneig,            nneig,            struc% tot_sites )
      !
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! The vector "ptr_site" is length "number_of_sites" and contains the current state of each site
      ! The vector "event_site" has the same configuration as "event_rate" and it stores the event index
      ! corresponding to the rates in the "event_rate" vector
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !
      j0 = nneig( is )
      jevt = event_site( j0 + jn )
      !
      IF ( site( is ) /= init_state( jevt ) ) THEN
         CALL warning( " Problem site state not correspond to initial state event ")
      ENDIF
      !
      site( is ) = final_state( jevt )
      !
    END SUBROUTINE event_applied
! .................................................................................................

    SUBROUTINE analyse( obj ) BIND( C )
      USE iso_c_binding
      USE derived_types
      USE sub_new_types
      USE variables
      IMPLICIT NONE
      !
      TYPE( KMC_type ), INTENT( inout ) :: obj
      INTEGER                           :: i, sum

      CALL link_int1_ptr( obj% ptr_site,             site,             obj% tot_sites )
      CALL link_real1_ptr( obj% ptr_prop,             prop,             obj% nprop )

      !
      ! The property that we want to compute is the number of sites that have a state 1
      sum = 0
      DO i = 1, obj%tot_sites
         sum = sum + site(i)
      END DO
      prop(1) = DBLE(sum) / DBLE(obj%tot_sites)
      !
    END SUBROUTINE analyse
! .................................................................................................
