## Excecise-1
* To run
```
make
```
* I am using the C code.
### Output for time
![](https://github.com/rjtkp/MonteCarlo/blob/master/D3/kMC/Ex01/frac_cov.png)

### Optimization

* I have tried incorporate the OpenMp version of paralalization but I didn't get any speed-up for 128 but for eveX 528n biggersize `528 x 528`. I didn't see any speed-up.

```
Without OpenMp

50 3.830512
128 25.966392
528 437.828670
```

* Also with using openmp on the outer loop for function `event_rate_calc` it's giving me 
```
Problem site state (1) does not correspond to initial state (0) event 0 PB choose_event... 
```
